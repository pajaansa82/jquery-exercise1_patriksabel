/*
 * Your name:Patrik Sabel
 * Start date:28.10.2019
 * End date:
 * 
 */

$(document).ready(function(){
    /**
     * Generates a random number in a min - max range
     * 
     * @param {Number} min  minimum value for a random number
     * @param {Number} max  maximum value for a random number
     * @returns {Number}    generated random number
     */
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1) ) + min;
    } 

    /**
     * Calculates the around of circle or square
     * 
     * @param {Number} value    radius or side measurement
     * @param {Number} type     1 = circle, 2 = square
     * @returns {String}        result of a calculation
     */
    function calculateAround(value, type) {
        let result = "";
        if (type === 1) {
            let around = 2 * Math.PI * value;
            result = "Circle around: " + around.toFixed(1);
        } else {
            let around = value * 4;
            result = "Square around: " + around.toFixed(1);
        }
        return result;
    }
   
    // dice images, use indexes 1 - 6
    let dice = [
        '',
        '<span><img src="img/noppa1.png" class="img-fluid" alt=""/></span>',
        '<span><img src="img/noppa2.png" class="img-fluid" alt=""/></span>',
        '<span><img src="img/noppa3.png" class="img-fluid" alt=""/></span>',
        '<span><img src="img/noppa4.png" class="img-fluid" alt=""/></span>',
        '<span><img src="img/noppa5.png" class="img-fluid" alt=""/></span>',
        '<span><img src="img/noppa6.png" class="img-fluid" alt=""/></span>'
    ];
    
    // red and blue flower
    let pair = [
        '<span><img src="img/flower1.png" class="img-fluid" alt=""/></span>',
        '<span><img src="img/flower2.png" class="img-fluid" alt=""/></span>'
    ];    

// Exercise 1. Circle or Square

    $("#calculate").click(function(){
        
        let type = Number($("input[type=radio][name=rad_cir_opt]:checked").val());
        let value = Number($("#radius_side").val());
        let print = calculateAround(value, type);   
        $("#result").html(print);

    });
    
    $("#radius_side").focusin(function(){
        $(this).select();
        $("#result").html("");
    });

// Exercise 2. Circle and Square    
    $(".rad_cir").click(function(){
       
        let open = $(this).attr("data-content");
        $(open).val("");
        
        if ($(this).prop("checked")=== true) {
            $(open).addClass("visible");
            $(open).removeClass("invisible");
            $(open).focus();
        } else {
            $(open).addClass("invisible");
            $(open).removeClass("visible");
        }
        
        let print =$(this).attr("data-result");
        $(print).html("");
        
        //alternatively
        // $(this).attr("data-result").html("");
        
    });
    
    $("#calculate2").click(function(){
       
        let boxes = $(".rad_cir");
        $(boxes).each(function(){
            if ($(this).prop("checked")=== true){
                let data = $(this).attr("data-content");
                let amount = Number($(data).val());
                //let amount = Number($($(this).attr("data-comtent")).val());
                let which_one = Number($(this).val());
                let res = calculateAround(amount, which_one);
                
//                let print =$(this).attr("data-result");
//                $(print).html(res);

                $($(this).attr("data-result")).html(res);
                
                //let some = 0;
            }
        });
        
    });
    
// Exercise 3. Random numbers 1   
     var count = 0;

    $("#numbers").click(function(){
      
        count++;
        let min = Number($("input[type=radio][name=num_scale]:checked").attr("data-min"));
        let max = Number($("input[type=radio][name=num_scale]:checked").attr("data-max")); 
        
        let print2 = getRndInteger(min, max);   
        $("#rnd_numbers").append(print2 + " ");
        $("#total").html(count);
    });

    $("#num_scale").focusin(function(){
        $(this).select();
        $("#rnd_numbers").html("");
        
    });

// Exercise 4. Random numbers 2           

    $("#random").click(function(){
    
        let min = Number($("#min").val());
        let max = Number($("#max").val());
        
        let i;
            for (i = 0; i < 10; i++) {
        
            let print3 = getRndInteger(min, max);   
            $("#randoms").append(print3 + " ");
            
        }
        
    });
    
    $(".min_max").focusin(function(){

        $(this).select();

        $("#randoms").html("");

    });
// Exercise 5. Throw dices    
    $("#throw").click(function(){

    
        let pairs = [0, 0, 0, 0, 0, 0, 0];
        
        $("#throw").focusin(function(){
            $(this).select();
            $("#dices").html("");
            $("#pairs").html("");

        });        
        
        let min = 1;

        let max = 6;  

 

        let j;

            for (j = 0; j < 100; j++) {

            let dice1 = getRndInteger(min, max);

            let dice2 = getRndInteger(min, max);
            
            
            
            if (dice1 === dice2)  {
                pairs[dice1]++;
                if (dice1 === 6)  { 
                    $("#dices").append("<li>" + dice[dice1] + dice[dice2] + pair[1] + "</li>");
                    $("#pairs").append(pairs);
                } else {
                    $("#dices").append("<li>" + dice[dice1] + dice[dice2] + pair[0] + "</li>");
                    $("#pairs").append(pairs);
                }

            } else { 
                $("#dices").append("<li>" + dice[dice1] + " " + dice[dice2] + "</li>");
            }

        }    

    });
});
